﻿using System.Data.Entity;
using DataLayer.Entities;

namespace DataLayer.Data
{
    public class DataDbContext : DbContext
    {
        public virtual DbSet<Author> Authors { get; set; }
        public virtual DbSet<Book> Books { get; set; }

        public DataDbContext(string contextName) : base(contextName)
        {

        }

    }
}
