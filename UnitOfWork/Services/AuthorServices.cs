﻿using System.Collections.Generic;
using System.Linq;
using DataLayer.Data;
using DataLayer.Repositories;
using DataLayer.Entities;

namespace DataLayer.Services
{
    public class AuthorServices
    {
        private DataDbContext _dbContext;
        private UnitOfWork _unitOfWork;

        public AuthorServices()
        {
            _dbContext = new DataDbContext("DataModel");
            _unitOfWork = new UnitOfWork(_dbContext);
        }

        public Author GetAuthorById(int id)
        {
            return _unitOfWork.AuthorRepository.Entities.FirstOrDefault(e => e.IndexId == id);
        }

        public ICollection<Author> GetAllAuthorsByName(string name)
        {
            return _unitOfWork.AuthorRepository.Entities.Where(e => e.Name == name).ToList<Author>();
        }

        public void AddAuthor(Author author)
        {
            _unitOfWork.AuthorRepository.Add(author);
            _unitOfWork.Commit();
        }

        public void UpdateAuthor(Author author)
        {
            Author authorRecord = GetAuthorById(author.IndexId);
            _unitOfWork.Commit();
        }

    }
}
