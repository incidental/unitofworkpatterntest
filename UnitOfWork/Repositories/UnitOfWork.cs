﻿using System;
using System.Data.Entity;
using System.Linq;
using DataLayer.Data;
using DataLayer.Entities;

namespace DataLayer.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DataDbContext _dbContext;

        public IRepository<Author> AuthorRepository => throw new NotImplementedException();

        public IRepository<Book> BooksRepository => new GenericRepository<Book>(_dbContext);

        public UnitOfWork(DataDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Commit()
        {
            _dbContext.SaveChanges();
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }

        public void RejectChanges()
        {
            foreach(var entry in _dbContext.ChangeTracker.Entries().Where(e => e.State != EntityState.Unchanged))
            {
                switch(entry.State)
                {
                    case EntityState.Added:
                        entry.State = EntityState.Deleted;
                        break;
                    case EntityState.Modified:
                    case EntityState.Deleted:
                        entry.Reload();
                        break;
                }
            }
        }
    }
}
