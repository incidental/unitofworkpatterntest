﻿using DataLayer.Entities;

namespace DataLayer.Repositories
{
    public interface IUnitOfWork
    {
        IRepository<Author> AuthorRepository { get; }
        IRepository<Book> BooksRepository { get; }

        void Commit();
        void RejectChanges();
        void Dispose();
    }
}
