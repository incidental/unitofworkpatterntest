﻿using System.Data.Entity;
using System.Linq;
using DataLayer.Data;

namespace DataLayer.Repositories
{
    public class GenericRepository<T> : IRepository<T> where T : class
    {
        private readonly DataDbContext _dbContext;

        private IDbSet<T> _dbSet => _dbContext.Set<T>();
        public IQueryable<T> Entities => _dbSet;

        public GenericRepository(DataDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Remove(T entity)
        {
            _dbSet.Remove(entity);
        }

        public void Add(T entity)
        {
            _dbSet.Add(entity);
        }
    }
}
